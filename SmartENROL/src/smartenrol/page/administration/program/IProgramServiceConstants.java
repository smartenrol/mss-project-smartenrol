package smartenrol.page.administration.program;

public interface IProgramServiceConstants {

    public static final String ID_PROGRAM = "ID_PROGRAM";
    public static final String ID_DEPARTMENT = "ID_DEPARTMENT";
    public static final String PROGRAM_NAME = "PROGRAM_NAME";
    public static final String PROGRAM_DESCRIPTION = "PROGRAM_DESCRIPTION";
    public static final String TOTAL_GRADE = "TOTAL_GRADE";  
}
