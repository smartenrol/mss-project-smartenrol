package smartenrol.page.administration.program;

import smartenrol.SmartEnrolException;

public class ProgramException extends SmartEnrolException {

    public ProgramException(final String errorMessage) {
        super(errorMessage);
    }
}
