package smartenrol.page.administration.department;

public interface IDepartmentServiceConstants
{
	public static final String ID_DEPARTMENT = "ID_DEPARTMENT";
	public static final String NAME = "NAME";
	public static final String ID_LOCATION = "ID_LOCATION";
	public static final String ID_FACULTY = "ID_FACULTY";
	public static final String DESCRIPTION = "DESCRIPTION";
	public static final String DEPARTMENT_HEAD_ID = "DEPARTMENT_HEAD_ID";
	public static final String MAIN_CONTACT_ID = "MAIN_CONTACT_ID";
}
