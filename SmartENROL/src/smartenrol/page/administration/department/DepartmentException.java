package smartenrol.page.administration.department;

import smartenrol.SmartEnrolException;

public class DepartmentException extends SmartEnrolException
{

	public DepartmentException(final String errorMessage)
	{
		super(errorMessage);
	}

}
