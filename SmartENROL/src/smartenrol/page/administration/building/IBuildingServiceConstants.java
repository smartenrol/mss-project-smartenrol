package smartenrol.page.administration.building;

public interface IBuildingServiceConstants {

    public static final String ID_LOCATION = "ID_LOCATION";
    public static final String NAME = "NAME";
    public static final String PROVINCE = "PROVINCE";
    public static final String ADDRESS_1 = "ADDRESS_1";
    public static final String ADDRESS_2 = "ADDRESS_2";
    public static final String CITY = "CITY";
    public static final String COUNTRY = "COUNTRY";
    public static final String POSTAL_CODE = "POSTAL_CODE";
    public static final String NOTES = "NOTES";
}
