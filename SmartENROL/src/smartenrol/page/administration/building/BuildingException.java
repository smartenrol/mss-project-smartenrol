package smartenrol.page.administration.building;

import smartenrol.SmartEnrolException;

public class BuildingException extends SmartEnrolException {

    public BuildingException(final String errorMessage) {
        super(errorMessage);
    }
}
