/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smartenrol;

/**
 *
 * @author Ashwin
 */
public class UniqueConstraintException extends SmartEnrolException {

    public UniqueConstraintException(final String errorMessage) {
        super(errorMessage);
    }
}
